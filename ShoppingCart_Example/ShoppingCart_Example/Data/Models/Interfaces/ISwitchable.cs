﻿using ShoppingCart_Example.Data.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Models.Interfaces
{
    interface ISwitchable
    {
        Status Status { set; get; }
    }
}
