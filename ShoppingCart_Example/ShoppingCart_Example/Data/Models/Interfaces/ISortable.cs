﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Models.Interfaces
{
    public interface ISortable
    {
        int SortOrder { set; get; }
    }
}
