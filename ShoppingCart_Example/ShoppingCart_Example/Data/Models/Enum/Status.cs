﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Models.Enum
{
    public enum Status
    {
        InActive = 0,
        Active = 1
    }
}
