﻿using ShoppingCart_Example.Data.Application.Interfaces;
using ShoppingCart_Example.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Extensions
{
    public static class CategoryExtensions
    {
        public static string GetFormattedBreadCrumb(this ProductCategoryViewModel category,
            IProductCategoryService categoryService,
            string separator = ">>")
        {
            var result = string.Empty;

            var breadcrumb = GetCategoryBreadCrumb(category, categoryService);
            for (var i = 0; i <= breadcrumb.Count - 1; i++)
            {
                var categoryName = breadcrumb[i].Name;
                result = string.IsNullOrEmpty(result)
                    ? categoryName
                    : $"{result} {separator} {categoryName}";
            }

            return result;
        }

        public static IList<ProductCategoryViewModel> GetCategoryBreadCrumb(this ProductCategoryViewModel category,
            IProductCategoryService categoryService)
        {
            if (category == null)
                throw new ArgumentNullException(nameof(category));

            var result = new List<ProductCategoryViewModel>();
            var alreadyProcessedCategoryIds = new List<int>();

            while (category != null &&
                !alreadyProcessedCategoryIds.Contains(category.Id))
            {
                result.Add(category);
                alreadyProcessedCategoryIds.Add(category.Id);
                if (category.ParentId.HasValue && category.Id != category.ParentId.Value)
                {
                    category = categoryService.GetById(category.ParentId.Value);
                }

            }
            result.Reverse();
            return result;
        }
    }
}
