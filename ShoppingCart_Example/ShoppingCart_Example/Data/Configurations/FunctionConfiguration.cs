﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ShoppingCart_Example.Data.Extensions;
using ShoppingCart_Example.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Configurations
{
    public class FunctionConfiguration : DbEntityConfiguration<Function>
    {
        public override void Configure(EntityTypeBuilder<Function> entity)
        {
            entity.HasKey(c => c.Id);
            entity.Property(c => c.Id).IsRequired()
            .IsUnicode(false).HasMaxLength(128);
            // etc.
        }
    }
}
