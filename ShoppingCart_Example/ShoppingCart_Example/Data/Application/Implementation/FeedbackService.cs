﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ShoppingCart_Example.Data.Application.Interfaces;
using ShoppingCart_Example.Data.Infrastructure;
using ShoppingCart_Example.Data.Models;
using ShoppingCart_Example.Utilities.Dtos;
using ShoppingCart_Example.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Application.Implementation
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IRepository<Feedback, int> _feedbackRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public FeedbackService(IRepository<Feedback, int> feedbackRepository,
            IUnitOfWork unitOfWork, IMapper mapper)
        {
            _feedbackRepository = feedbackRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Add(FeedbackViewModel feedbackVm)
        {
            var page = _mapper.Map<FeedbackViewModel, Feedback>(feedbackVm);
            _feedbackRepository.Add(page);
        }

        public void Delete(int id)
        {
            _feedbackRepository.Remove(id);
        }
        
        public List<FeedbackViewModel> GetAll()
        {
            var query = _feedbackRepository.FindAll().ToList();
            var feedbackVms = _mapper.Map<List<Feedback>, List<FeedbackViewModel>>(query);
            return feedbackVms;
        }

        public PagedResult<FeedbackViewModel> GetAllPaging(string keyword, int page, int pageSize)
        {
            var query = _feedbackRepository.FindAll();
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword));

            int totalRow = query.Count();
            var data = query.OrderByDescending(x => x.DateCreated)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            var feedbackVms = _mapper.Map<IEnumerable<Feedback>, IEnumerable<FeedbackViewModel>>(data).ToList();

            var paginationSet = new PagedResult<FeedbackViewModel>()
            {
                Results = feedbackVms,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public FeedbackViewModel GetById(int id)
        {
            return _mapper.Map<Feedback, FeedbackViewModel>(_feedbackRepository.FindById(id));
        }

        public void SaveChanges()
        {
            _unitOfWork.Commit();
        }

        public void Update(FeedbackViewModel feedbackVm)
        {
            var page = _mapper.Map<FeedbackViewModel, Feedback>(feedbackVm);
            _feedbackRepository.Update(page);
        }
    }
}
