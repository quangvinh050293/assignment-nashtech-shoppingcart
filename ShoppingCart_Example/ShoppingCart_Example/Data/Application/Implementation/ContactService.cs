﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using ShoppingCart_Example.Data.Application.Interfaces;
using ShoppingCart_Example.Data.Infrastructure;
using ShoppingCart_Example.Data.Models;
using ShoppingCart_Example.Utilities.Dtos;
using ShoppingCart_Example.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Application.Implementation
{
    public class ContactService : IContactService
    {
        private readonly IRepository<Contact, string> _contactRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ContactService(IRepository<Contact, string> contactRepository,
            IUnitOfWork unitOfWork, IMapper mapper)
        {
            _contactRepository = contactRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Add(ContactViewModel contactVm)
        {
            var page = _mapper.Map<ContactViewModel, Contact>(contactVm);
            _contactRepository.Add(page);
        }

        public void Delete(string id)
        {
            _contactRepository.Remove(id);
        }
        
        public List<ContactViewModel> GetAll()
        {
            var query = _contactRepository.FindAll().ToList();
            var contactVms = _mapper.Map<List<Contact>, List<ContactViewModel>>(query);
            return contactVms;
        }

        public PagedResult<ContactViewModel> GetAllPaging(string keyword, int page, int pageSize)
        {
            var query = _contactRepository.FindAll();
            if (!string.IsNullOrEmpty(keyword))
                query = query.Where(x => x.Name.Contains(keyword));

            int totalRow = query.Count();
            var data = query.OrderByDescending(x => x.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize);
            var contactVms = _mapper.Map<IEnumerable<Contact>, IEnumerable<ContactViewModel>>(data).ToList();
            var paginationSet = new PagedResult<ContactViewModel>()
            {
                Results = contactVms,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public ContactViewModel GetById(string id)
        {
            return _mapper.Map<Contact, ContactViewModel>(_contactRepository.FindById(id));
        }

        public void SaveChanges()
        {
            _unitOfWork.Commit();
        }

        public void Update(ContactViewModel contactVm)
        {
            var page = _mapper.Map<ContactViewModel, Contact>(contactVm);
            _contactRepository.Update(page);
        }
    }
}
