﻿using ShoppingCart_Example.ViewModels.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Application.Interfaces
{
    public interface IFunctionService
    {
        void Add(FunctionViewModel function);
        Task<List<FunctionViewModel>> GetAll(string filter);
        FunctionViewModel GetById(string id);
        void Update(FunctionViewModel function);
        void Delete(string id);
        void Save();
        void UpdateParentId(string sourceId, string targetId, Dictionary<string, int> items);
        void ReOrder(string sourceId, string targetId);
    }
}
