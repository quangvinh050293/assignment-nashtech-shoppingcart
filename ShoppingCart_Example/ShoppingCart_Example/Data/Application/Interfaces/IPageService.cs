﻿using ShoppingCart_Example.Utilities.Dtos;
using ShoppingCart_Example.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Data.Application.Interfaces
{
    public interface IPageService
    {
        void Add(PageViewModel pageVm);
        void Update(PageViewModel pageVm);
        void Delete(int id);
        List<PageViewModel> GetAll();
        PagedResult<PageViewModel> GetAllPaging(string keyword, int page, int pageSize);
        PageViewModel GetByAlias(string alias);
        PageViewModel GetById(int id);
        void Save();
    }
}
