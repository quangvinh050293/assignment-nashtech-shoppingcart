﻿var CategoryController = function () {
    this.initialize = function () {
        loadData(true);
        registerEvents();
    }

    function registerEvents() {
        //todo: binding events to controls

        $('#ddlShowPage').on('change', function (e) {
            common.configs.pageSize = $(this).val();
            common.configs.pageIndex = 1;
            loadData(true);
        });

        $('#btnSearch').on('click', function (e) {
            loadData();
        });
        $('#txtKeyword').on('keypress', function (e) {
            if (e.which === 13) {
                loadData();
            }
        });

        $('body').on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            deleteProduct(id);
        });
    }

    function loadData(isPageChanged) {
        var template = $('#table-template').html();
        var render = "";
        $.ajax({
            type: 'GET',
            data: {
                keyword: $('#txtKeyword').val(),
                page: common.configs.pageIndex,
                pageSize: common.configs.pageSize
            },
            url: '/Admin/ProductCategory/GetAllPaging',
            beforeSend: function () {
                common.startLoading();
            },
            success: function (res) {
                common.stopLoading();
                $.each(res.Results, function (i, item) {
                    render += Mustache.render(template, {
                        Id: item.Id,
                        Name: item.Name,
                        DateCreated: common.dateTimeFormatJson(item.DateCreated),
                        DateModified: common.dateTimeFormatJson(item.DateModified),
                        DisplayOrder: item.SortOrder,
                        Status: common.getStatus(item.Status)
                    });
                });

                $('#lblTotalRecords').text(res.RowCount);

                if (render !== '') {
                    $('#tbl-content').html(render);
                } else {
                    $('#tbl-content').html(render);
                    common.notify(Message.NotFound);
                }
                if (res.Results.length <= 0) {
                    common.configs.pageIndex = 1;
                    loadData(true);
                } else {
                    wrapPaging(res.RowCount, function () {
                        loadData();
                    }, isPageChanged);
                }
            },
            error: function (status) {
                console.log(status);
                common.notify('Cannot loading data', 'error');
                common.stopLoading();
            }
        });
    }

    function wrapPaging(recordCount, callback, changePageSize) {
        var totalsize = Math.ceil(recordCount / common.configs.pageSize);
        //Unbind pagination if it existed or click change pagesize
        if ($('#paginationUL a').length === 0 || changePageSize === true) {
            $('#paginationUL').empty();
            $('#paginationUL').removeData("twbs-pagination");
            $('#paginationUL').unbind("page");
        }
        //Bind Pagination Event
        $('#paginationUL').twbsPagination({
            totalPages: totalsize,
            visiblePages: 7,
            initiateStartPageClick: false,
            first: 'First',
            prev: 'Previous',
            next: 'Next',
            last: 'Last',
            onPageClick: function (event, p) {
                common.configs.pageIndex = p;
                setTimeout(callback, 200);
            }
        });
    }

    function deleteProduct(id) {
        common.confirm('Are you sure to delete?', function () {
            $.ajax({
                type: "POST",
                url: "/Admin/Category/Delete",
                data: { id: id },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    common.notify('Delete successful', 'success');
                    common.stopLoading();
                    loadData();
                },
                error: function (status) {
                    common.notify('Has an error in delete progress', 'error');
                    common.stopLoading();
                }
            });
        });
    }
}