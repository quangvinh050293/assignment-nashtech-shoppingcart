﻿var CategoryController = function () {
    this.initialize = function () {
        registerEvents();
    }

    function registerEvents() {
        //todo: binding events to controls
        $('#frmMaintainance').validate({
            errorClass: 'red',
            ignore: [],
            lang: 'vi',
            rules: {
                txtNameM: { required: true },
                ddlCategoryIdM: { required: true },
                txtSeoKeywords: { required: true },
                txtSeoDescription: { required: true }
            }
        });



        $('#btnSave').on('click', function (e) {
            SaveEntity(e);
        });
    }

    function SaveEntity(e) {
        if ($('#frmMaintainance').valid()) {
            e.preventDefault();
            var id = $('#hidIdM').val();
            var name = $('#txtNameM').val();
            var parentId = $('#ddlCategoryIdM').val();
            var description = $('#txtDescM').val();
            var sortOrder = $('#txtSortOrder').val();
            var seoKeyword = $('#txtSeoKeywords').val();
            var seoDescription = $('#txtSeoDescription').val();
            var status = $('#ckStatusM').prop('checked') == true ? 1 : 0;
            var showHome = $('#ckShowHomeM').prop('checked');

            var productCategoryVm = {
                Id: id,
                Name: name,
                Description: description,
                ParentId: parentId,
                SortOrder: sortOrder,
                Status: status,
                SeoKeywords: seoKeyword,
                SeoDescription: seoDescription,
                HomeFlag: showHome
            }

            $.ajax({
                type: "POST",
                url: "/Admin/ProductCategory/SaveEntity",
                data: {
                    productCategoryVm: productCategoryVm
                },
                dataType: "json",
                beforeSend: function () {
                    common.startLoading();
                },
                success: function (response) {
                    common.notify('Update category successful', 'success');
                    common.stopLoading();
                    window.location.href = Defines.CategoryAdmin;
                },
                error: function () {
                    common.notify('Has an error in save category progress', 'error');
                    common.stopLoading();
                }
            });
            return false;
        }
    }
}