﻿using ShoppingCart_Example.Data.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.ViewModels.Common
{
    public class FeedbackViewModel
    {
        public int Id { set; get; }
        [StringLength(250)]
        [Required]
        public string Name { set; get; }

        [StringLength(250)]
        public string Email { set; get; }

        [StringLength(500)]
        public string Message { set; get; }

        public Status Status { set; get; }
        public DateTime? DateCreated { set; get; }
        public DateTime? DateModified { set; get; }
    }
}
