﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.ViewModels.Common
{
    public class TagViewModel
    {
        public string Id { set; get; }

        public string Name { set; get; }

        public string Type { set; get; }
    }
}
