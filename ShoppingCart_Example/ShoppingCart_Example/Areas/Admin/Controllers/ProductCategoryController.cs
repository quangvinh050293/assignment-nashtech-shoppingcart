﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using ShoppingCart_Example.Data.Application.Interfaces;
using ShoppingCart_Example.Data.Extensions;
using ShoppingCart_Example.Data.Models;
using ShoppingCart_Example.Utilities.Dtos;
using ShoppingCart_Example.Utilities.Helpers;
using ShoppingCart_Example.ViewModels.Product;

namespace ShoppingCart_Example.Areas.Admin.Controllers
{
    public class ProductCategoryController : BaseController
    {
        private readonly IProductCategoryService _productCategoryService;
        public ProductCategoryController(IProductCategoryService productCategoryService)
        {
            _productCategoryService = productCategoryService;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            var productCategoryModel = new ProductCategoryViewModel();
            PrepareCategoryMapping(productCategoryModel);
            return View(productCategoryModel);
        }

        public IActionResult Edit(int id)
        {
            var productCategoryModel = _productCategoryService.GetById(id);
            PrepareCategoryMapping(productCategoryModel);
            return View(productCategoryModel);
        }

        #region Ajax
        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _productCategoryService.GetAll();
            return new OkObjectResult(model);
        }
        [HttpGet]
        public IActionResult GetAllPaging(string keyword, int page, int pageSize)
        {
            var categories = _productCategoryService.GetAllPaging(keyword, page, pageSize);
            var model = new PagedResult<ProductCategoryViewModel>()
            {
                Results = categories.Results.Select(x =>
                {
                    var categoryModel = x;
                    categoryModel.Name = x.GetFormattedBreadCrumb(_productCategoryService);
                    return categoryModel;
                }).ToList(),
                CurrentPage = page,
                RowCount = categories.RowCount,
                PageSize = pageSize
            };

            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            var model = _productCategoryService.GetById(id);
            return new ObjectResult(model);
        }
        [HttpPost]
        public IActionResult SaveEntity(ProductCategoryViewModel productCategoryVm)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                productCategoryVm.SeoAlias = TextHelper.ToUnsignString(productCategoryVm.Name);
                if (productCategoryVm.Id == 0)
                {
                    _productCategoryService.Add(productCategoryVm);
                }
                else
                {
                    _productCategoryService.Update(productCategoryVm);
                }
                _productCategoryService.Save();
                return new OkObjectResult(productCategoryVm);
            }
        }
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new BadRequestResult();
            }
            else
            {
                _productCategoryService.Delete(id);
                _productCategoryService.Save();
                return new OkObjectResult(id);
            }
        }
        #endregion

        #region Mapping to UI
        public void PrepareCategoryMapping(ProductCategoryViewModel model)
        {
            var allCategory = _productCategoryService.GetAll().Where(c => c.Id != model.Id);
            model.ParentIds.Add(new SelectListItem
            {
                Text = "--Select category--",
                Value = "",
                Selected = model.ParentId.HasValue ? true : false
            });
            foreach (var category in allCategory)
            {
                model.ParentIds.Add(new SelectListItem
                {
                    Text = category.Name,
                    Value = category.Id.ToString(),
                    Selected = model.ParentId.HasValue ? true : false
                });
            }

            model.StatusFlag = (int)model.Status == 1 ? true : false;
            if (model.Id == 0)
            {
                model.HomeFlag = false;
            }
            
        }
        #endregion
    }
}