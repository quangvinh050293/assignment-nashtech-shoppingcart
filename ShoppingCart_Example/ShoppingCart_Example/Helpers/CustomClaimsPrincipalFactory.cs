﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using ShoppingCart_Example.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Helpers
{
    public class CustomClaimsPrincipalFactory : UserClaimsPrincipalFactory<AppUser, AppRole>
    {
        UserManager<AppUser> _userManager;
        public CustomClaimsPrincipalFactory(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager
            , IOptions<IdentityOptions> options) : base(userManager, roleManager, options)
        {
            _userManager = userManager;
        }

        public async override Task<ClaimsPrincipal> CreateAsync(AppUser user)
        {
            var principal = await base.CreateAsync(user);
            var roles = await _userManager.GetRolesAsync(user);
            ((ClaimsIdentity)principal.Identity).AddClaims(new[]
            {
                new Claim("Email", user.Email),
                new Claim("FullName",user.FullName),
                new Claim("Avatar",user.Avatar ?? string.Empty),
                new Claim("Roles",string.Join(";",roles)),
                new Claim("DateCreated",user.DateCreated.Value.ToString("MMM.yyyy"))
            });
            return principal;
        }
    }
}
