﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingCart_Example.Utilities.Constants
{
    public class CommonConstants
    {
        public const string CartSession = "CartSession";
        public const string ProductTag = "Product";
        public class AppRole
        {
            public const string AdminRole = "Admin";
        }
        public class UserClaims
        {
            public const string Roles = "Roles";
        }
    }
}
